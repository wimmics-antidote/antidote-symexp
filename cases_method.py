"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import re
import requests


def extract_symptoms(ner_prediction):
    """
    Function to create a list with all symptoms from the ner prediction

    Parameters
    ==========
    ner_prediction: List[List[str, str]]
        The list of prediction from the ner, which is a list of list with two string

    Returns
    =======
    List[str]
        The list of symptoms
    """
    symptoms = []
    current_symptoms = []
    regex = re.compile(r"[^a-zA-Z\s\-]+|[\.,]+")

    # We assemble back the symptoms
    for token, detected in ner_prediction:
        # Check if it's the Beginning of a new symptom
        if detected == "B-Sign_or_Symptom":
            if current_symptoms:
                symptoms.append(regex.sub("", " ".join(current_symptoms)))
                current_symptoms = []
            current_symptoms.append(token)
        # Check if it's the Inside of a symptom
        elif detected == "I-Sign_or_Symptom":
            current_symptoms.append(token)
        else:
            # Check if there is a symptom waiting to be added
            if current_symptoms:
                symptoms.append(regex.sub("", " ".join(current_symptoms)))
                current_symptoms = []
    # If there is still a symptom that was not added, we add it to the list of symptoms
    if current_symptoms:
        symptoms.append(regex.sub("", " ".join(current_symptoms)))
    return symptoms


# Function to get only the findings
def extract_findings(ner_prediction):
    """
    Function to create a list with all findings from the ner prediction

    Parameters
    ==========
    ner_prediction: List[List[str, str]]
        The list of prediction from the ner, which is a list of list with two string

    Returns
    =======
    List[str]
        The list of findings
    """
    findings = []
    current_finding = []

    # We assemble back the findings
    for token, detected in ner_prediction:
        # Check if it's the Beginning of a new finding
        if detected == "B-Finding":
            if current_finding:
                findings.append(" ".join(current_finding))
                current_finding = []
            current_finding.append(token)
        # Check if it's the Inside of a symptom
        elif detected == "I-Finding":
            current_finding.append(token)
        else:
            # Check if there is a finding waiting to be added
            if current_finding:
                findings.append(" ".join(current_finding))
                current_finding = []
    # If there is still a finding that was not added, we add it to the list of findings
    if current_finding:
        findings.append(" ".join(current_finding))

    return findings


def list_labels(ner_prediction):
    """
    Function to create a list with word labels

    Parameters
    ==========
    ner_prediction: List[List[str, str]]
        The list of prediction from the ner, which is a list of list with two string

    Returns
    =======
    List[str]
        The list composed of the word labels
    """
    word_labels = []
    for token, detected in ner_prediction:
        word_labels.append(detected)
    return word_labels


def search_synonyms(term):
    api_key = "897c33a1-9ded-4760-a3a9-25d0aa061d1a"
    base_uri = "https://uts-ws.nlm.nih.gov/rest"
    version = "current"
    # First, we retrieve CUI for the term
    response = requests.get(f"{base_uri}/search/{version}?string={term}&apiKey={api_key}")
    response_json = response.json()
    cui = response_json["result"]["results"][0]["ui"]

    # Then, retrieve the atoms (including synonyms) for the CUI
    response = requests.get(f"{base_uri}/content/{version}/CUI/{cui}/atoms?apiKey={api_key}")
    response_json = response.json()

    # Filter for English synonyms
    synonyms = [result["name"] for result in response_json["result"] if result["language"] == "ENG"]
    return synonyms


def search_synonyms_hpo(symptom):
    base_url = "https://hpo.jax.org/api/hpo"
    endpoint = "/search"
    url = f"{base_url}{endpoint}?q={symptom}"

    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        id = data["terms"][0]["id"]
    else:
        return response.status_code

    response = requests.get(f"{base_url}/term/{id}")
    if response.status_code == 200:
        data = response.json()
        return data["details"]["synonyms"]
    else:
        return response.status_code
