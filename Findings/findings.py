"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import logging
from openai import OpenAI

import pandas as pd
import os
import random
import string
import re
import time

logger = logging.getLogger(__name__)


def main(findings, key):
    """
    Main function of the findings module

    Parameters
    ==========
    findings: List[Str]
        List of findings to use
    key: Str
        Key for the OpenAI api

    Returns
    =======
    list_gold_labels: List[Str]
        List of labels matched from findings
    """
    list_gold_labels, no_match_findings = get_findings_in_database(findings)
    if no_match_findings and key:
        client = OpenAI(api_key=key)
        for finding in no_match_findings:
            logger.debug(finding)
            pred = robust_get_medical_term(finding, client)
            logger.debug(pred)
            list_gold_labels.append(pred)
    return list_gold_labels


def get_seed():
    """
    Generates a random alphanumeric string of a specified length to be used as a seed.

    Returns:
        str: A random alphanumeric string.
    """
    # Define the length of the random string
    length = 10

    # Generate the random string
    random_string = "".join(
        random.choice(string.ascii_letters + string.digits) for _ in range(length)
    )

    # Return the random string
    return random_string


def prompt(finding, client):
    """
    Function to prepare the GPT model and ask it a prompt

    Parameters
    ==========
    finding: List[Str]
        List of finding
    client: OpenAI
        Used to send request to the OpenAI API

    Returns
    =======
    response_text: Str
        Response text gotten from the GPT model
    """
    prompt_system = """Ignore all instructions before this one. You’re a doctor assistant. You have been doing medicine for 20 years. Your task is now to return the medical terms associated to findings.
    ---
    """  # noqa

    prompt_prefix = """Fill the following table by replacing "?":

    | Finding | Medical term |
    | [FINDING] | ? |

    ONLY fill the table with ONE line, no extra sentences
    Put "-" if the value is normal """
    prompt_ = prompt_prefix.replace("[FINDING]", finding)
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": prompt_system},
            {"role": "user", "content": prompt_},
        ],
    )
    response_text = response.choices[0].message.content
    logger.debug(response_text)
    return response_text


def get_medical_term(s):
    """
    Parameters
    ==========
    s: str
        The entire answer that we got from the GPT model

    Returns
    =======
    last_column: str
        The medical term associated with the finding
    """
    lines = s.strip().split("\n")  # split the input into lines
    last_line = None
    for line in reversed(lines):  # loop through the lines from the end
        if line.strip():  # if the line is not empty
            last_line = line
            break
    if last_line is None:  # if all lines are empty
        raise ValueError("No non-empty lines found.")
    columns = last_line.split("|")  # split the last line into columns
    last_column = None
    for column in reversed(columns):  # loop through the columns from the end
        if column.strip():  # if the column is not empty
            last_column = column
            break
    if last_column is None:  # if all columns are empty
        raise ValueError(f"No non-empty columns found in the line: {last_line}")
    return last_column.strip()  # return the last column (and strip any surrounding whitespace)


def robust_get_medical_term(finding, client):
    """
    Attemps to retrieve a medical term from the GPT model

    Parameters
    ----------
    finding: str
        The finding used for the prompt
    client: OpenAI
        The OpenAI API client

    Returns
    -------
    str
        The medical term associated with the finding
    """
    while True:
        logger.debug(".")
        try:
            return get_medical_term(prompt(finding, client))
        except Exception as e:
            print(e)
            logger.error("EXCEPTION :", e)
            logger.error("Rate limit exceeded. Sleeping for 60 seconds.")
            time.sleep(60)


def get_findings_in_database(findings_ner):
    """
    Function used to align findings with the database csv

    Parameters
    ==========
    findings_ner: List[str]
        List of findings to use

    Returns
    =======
    list_gold_labels: List[str]
        List of gold labels matched
    no_match_findings: List[str]
        List of findings without match from the database
    """
    list_gold_labels = []
    no_match_findings = []
    current_dir = os.path.dirname(__file__)
    data_path = os.path.join(current_dir, "resources", "Database.csv")
    database = pd.read_csv(data_path)

    for finding_ner in findings_ner:
        found = False
        for finding in database["Finding"]:
            if str.lower(finding) in str.lower(finding_ner):
                found = True
                # We get the values from the database of the finding
                matched_row = database.loc[database["Finding"] == finding]
                unit = matched_row["Unit"].values[0]
                goldLow = matched_row["Gold Low Term"].values[0]
                goldHigh = matched_row["Gold High Term"].values[0]
                lowValue = matched_row["Low value Boundary"].values[0]
                highValue = matched_row["High value Boundary"].values[0]

                if str.lower(unit) in str.lower(finding_ner):
                    # We need to check for blood pressure because it's a special unit "n/x"
                    if finding == "Blood pressure":
                        value_finding = re.findall(r"(?<!\d)(\d+(?:[.,]\d+)*)(?!\d)", finding_ner)
                        systolic1, diastolic1 = value_finding
                        systolicLow, diastolicLow = lowValue.split("/")
                        systolicHigh, diastolicHigh = highValue.split("/")
                        if systolic1 <= systolicLow and diastolic1 <= diastolicLow:
                            list_gold_labels.append(goldLow)
                        elif systolic1 >= systolicHigh and diastolic1 >= diastolicHigh:
                            list_gold_labels.append(goldHigh)
                    else:
                        value_finding = re.findall(r"(?<!\d)(\d+(?:[.,]\d+)*)(?!\d)", finding_ner)[
                            0
                        ]
                        if value_finding >= highValue:
                            list_gold_labels.append(goldHigh)
                        elif value_finding <= lowValue:
                            list_gold_labels.append(goldLow)
                else:
                    no_match_findings.append(finding_ner)
        if not found:
            no_match_findings.append(finding_ner)

    return list_gold_labels, no_match_findings
