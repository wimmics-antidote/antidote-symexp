from sqlalchemy import create_engine, Column, String, Integer, ForeignKey
from sqlalchemy.orm import declarative_base, relationship, sessionmaker
import os
import xmltodict
import json
from tqdm.auto import tqdm


def xml_to_json(xml_file, json_file):
    with open(xml_file, "rt", encoding="ISO-8859-1") as file:
        xml_content = file.read()

    xml_dict = xmltodict.parse(xml_content)
    json_data = json.dumps(xml_dict, indent=4)

    with open(json_file, "w") as file:
        file.write(json_data)


def remove_unwanted_elements(data):
    if isinstance(data, dict):
        if "DisorderGroup" in data and isinstance(data["DisorderGroup"], dict):
            disorder_group = data["DisorderGroup"]
            if (
                "@id" in disorder_group
                and "Name" in disorder_group
                and isinstance(disorder_group["Name"], dict)
                and "@lang" in disorder_group["Name"]
                and "#text" in disorder_group["Name"]
            ):
                del data["DisorderGroup"]

        if "DisorderType" in data and isinstance(data["DisorderType"], dict):
            disorder_type = data["DisorderType"]
            if (
                "@id" in disorder_type
                and "Name" in disorder_type
                and isinstance(disorder_type["Name"], dict)
                and "@lang" in disorder_type["Name"]
                and "#text" in disorder_type["Name"]
            ):
                del data["DisorderType"]

        if "ExpertLink" in data and isinstance(data["ExpertLink"], dict):
            expert_link = data["ExpertLink"]
            if "@lang" in expert_link and "#text" in expert_link:
                del data["ExpertLink"]

        for key in ["Source", "ValidationStatus", "Online", "ValidationDate"]:
            if key in data:
                del data[key]

        for key in list(data.keys()):
            remove_unwanted_elements(data[key])
    elif isinstance(data, list):
        for item in data:
            remove_unwanted_elements(item)


def clean_json(input_file, output_file):
    with open(input_file, "r") as file:
        data = json.load(file)

    remove_unwanted_elements(data)

    with open(output_file, "w") as file:
        json.dump(data, file, indent=4)
    os.remove(input_file)


def collect_hpo_terms_and_frequencies(data, hpo_terms, hpo_frequencies):
    if isinstance(data, dict):
        if "HPO" in data and isinstance(data["HPO"], dict):
            hpo_term = data["HPO"].get("HPOTerm")
            if hpo_term:
                hpo_terms.append(hpo_term)

        if "HPOFrequency" in data and isinstance(data["HPOFrequency"], dict):
            name = data["HPOFrequency"].get("Name", {})
            hpo_frequency_text = name.get("#text")
            if hpo_frequency_text:
                hpo_frequencies.append(hpo_frequency_text)

        for key in data:
            collect_hpo_terms_and_frequencies(data[key], hpo_terms, hpo_frequencies)
    elif isinstance(data, list):
        for item in data:
            collect_hpo_terms_and_frequencies(item, hpo_terms, hpo_frequencies)


def find_hpodosorder_association_list(data, disorder_name):
    disorder_name_lower = disorder_name.lower()
    if isinstance(data, dict):
        if "Disorder" in data and isinstance(data["Disorder"], dict):
            name = data["Disorder"].get("Name", {})
            if name.get("@lang") == "en" and name.get("#text", "").lower() == disorder_name_lower:
                return data["Disorder"].get("HPODisorderAssociationList")

        for key in data:
            result = find_hpodosorder_association_list(data[key], disorder_name)
            if result is not None:
                return result
    elif isinstance(data, list):
        for item in data:
            result = find_hpodosorder_association_list(item, disorder_name)
            if result is not None:
                return result

    return None


def collect_diseases_and_associations(data):
    diseases = set()
    associations = set()
    hpo_codes = set()

    def collect(data):
        if isinstance(data, dict):
            if "Disorder" in data and isinstance(data["Disorder"], dict):
                orpha_code = data["Disorder"].get("OrphaCode")
                name = data["Disorder"].get("Name", {}).get("#text")
                if orpha_code and name:
                    diseases.add((orpha_code, name))
                hpo_list = (
                    data["Disorder"]
                    .get("HPODisorderAssociationList", {})
                    .get("HPODisorderAssociation", [])
                )
                if isinstance(hpo_list, list):
                    for hpo in hpo_list:
                        hpo_code = hpo.get("HPO", {}).get("HPOId")
                        hpo_term = hpo.get("HPO", {}).get("HPOTerm")
                        if hpo_code and hpo_term:
                            hpo_codes.add(hpo_code)
                            associations.add((hpo_code, hpo_term))
            for key in data:
                collect(data[key])
        elif isinstance(data, list):
            for item in data:
                collect(item)

    collect(data)
    return list(diseases), list(associations)


def collect_diseases_and_associations_with_frequency(data):
    results = []

    def collect(data, disease_name=None):
        if isinstance(data, dict):
            if "Disorder" in data and isinstance(data["Disorder"], dict):
                disease_name = data["Disorder"].get("Name", {}).get("#text")
                hpo_list = (
                    data["Disorder"]
                    .get("HPODisorderAssociationList", {})
                    .get("HPODisorderAssociation", [])
                )
                if isinstance(hpo_list, list) and disease_name:
                    for hpo in hpo_list:
                        hpo_term = hpo.get("HPO", {}).get("HPOTerm")
                        frequency = hpo.get("HPOFrequency", {}).get("Name", {}).get("#text")
                        if hpo_term and frequency:
                            results.append((disease_name, hpo_term, frequency))
            for key in data:
                collect(data[key], disease_name)
        elif isinstance(data, list):
            for item in data:
                collect(item, disease_name)

    collect(data)
    return results


def process_json(input_file):
    with open(input_file, "r") as file:
        data = json.load(file)

    diseases, associations = collect_diseases_and_associations(data)
    associations_freq = collect_diseases_and_associations_with_frequency(data)

    return diseases, associations, associations_freq


Base = declarative_base()


class Disease(Base):
    __tablename__ = "diseases"
    orphaID = Column(String(10), primary_key=True, nullable=False)
    name_d = Column(String(100), nullable=False)


class Symptom(Base):
    __tablename__ = "symptoms"
    hpID = Column(String(9), primary_key=True, nullable=False)
    name_s = Column(String(100), nullable=False)


class DiseaseSymptom(Base):
    __tablename__ = "disease_symptom"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name_d = Column(String(100), ForeignKey("diseases.name_d"), nullable=False)
    name_s = Column(String(100), ForeignKey("symptoms.name_s"), nullable=False)
    frequence = Column(String(20), nullable=False)

    disease = relationship("Disease", backref="symptom_associations")
    symptom = relationship("Symptom", backref="disease_associations")


def get_symptoms_for_disease(session, disease_name):
    results = session.query(DiseaseSymptom).filter(DiseaseSymptom.name_d == disease_name).all()
    list_symptom = [result.name_s for result in results]
    list_frequence = [result.frequence for result in results]
    return list_symptom, list_frequence


def get_symptoms_for_diseases(list_diseases):
    engine = create_engine("sqlite:///HPO.db")
    Session = sessionmaker(bind=engine)
    session = Session()

    list_diseases_symptoms = {}
    for disease in list_diseases:
        list_symptoms, list_frequence = get_symptoms_for_disease(session, disease)
        list_diseases_symptoms[disease] = {
            "list_symptoms": list_symptoms,
            "list_frequence": list_frequence,
        }
    session.close()
    return list_diseases_symptoms


def searchDiseaseByInclusion(input, limit=5):
    engine = create_engine("sqlite:///HPO.db")
    Session = sessionmaker(bind=engine)
    session = Session()
    results = session.query(Disease).filter(Disease.name_d.ilike(f"%{input}%")).limit(limit).all()
    disease_list = []
    for result in results:
        disease_list.append({"disease": result.name_d, "id": result.orphaID})
    session.close()
    return disease_list


def create_json(xml_path, json_path, clean_json_path):
    if os.path.isfile(json_path):
        os.remove(json_path)
    if os.path.isfile(clean_json_path):
        os.remove(clean_json_path)
    xml_to_json(xml_path, json_path)
    clean_json(json_path, clean_json_path)


def create_db(clean_json_path):
    engine = create_engine("sqlite:///HPO.db")
    Session = sessionmaker(bind=engine)
    session = Session()

    Base.metadata.create_all(engine)

    session.query(DiseaseSymptom).delete()
    session.query(Symptom).delete()
    session.query(Disease).delete()
    session.commit()

    diseases, associations, freq = process_json(clean_json_path)

    for orphaID, name_d in tqdm(diseases):
        if not session.query(Disease).filter_by(orphaID=orphaID).first():
            disease = Disease(orphaID=orphaID, name_d=name_d)
            session.add(disease)

    for hpID, name_s in tqdm(associations):
        if not session.query(Symptom).filter_by(hpID=hpID).first():
            symptom = Symptom(hpID=hpID, name_s=name_s)
            session.add(symptom)

    for name_d, name_s, frequence in tqdm(freq):
        disease_symptom = DiseaseSymptom(name_d=name_d, name_s=name_s, frequence=frequence)
        session.add(disease_symptom)

    session.commit()
    session.close()
