"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import logging
import torch


logger = logging.getLogger(__name__)


def main(cases, model, tokenizer):
    """
    Main function for the ner

    Parameters
    ==========
    cases: List[str]
        List of medical cases
    model: BertForTokenClassification
        Model used for the prediction
    tokenizer: BertTokenizerFast
        Tokenizer used for the prediction

    Returns
    =======
    list_cases: List[str]
        List of medical cases
    list_predictions: List[List[str,str]]
        List of prediction made by the ner
    """
    list_cases = []
    list_predictions = []
    for case in cases:
        case = case.strip()
        logger.debug(case)
        prediction = single_case_ner(case, model, tokenizer)
        list_cases.append(case)
        list_predictions.append(prediction)

    return list_cases, list_predictions


def single_case_ner(case, model, tokenizer):
    """
    Function used to make prediction on one medical case
    Main function for the ner

    Parameters
    ==========
    case: str
        Medical case
    model: BertForTokenClassification
        Model used for the prediction
    tokenizer: BertTokenizerFast
        Tokenizer used for the prediction

    Returns
    =======
    list_cases: List[str]
        List of medical cases
    list_predictions: List[List[str,str]]
        List of prediction made by the ner
    """
    case = case.strip().split()
    # Define the labels
    # And the correspondence
    ids_to_labels = {
        0: "O",
        1: "B-Age_Group",
        2: "B-Population_Group",
        3: "B-Relevant_symptoms",
        4: "B-Temporal_Concept",
        5: "I-Temporal_Concept",
        6: "B-No_Symptom_Occurence",
        7: "B-Finding",
        8: "I-Finding",
        9: "I-Relevant_symptoms",
        10: "B-Sign_or_Symptom",
        11: "I-Sign_or_Symptom",
        12: "B-Location",
        13: "B-No_Finding_Occurence",
        14: "I-Location",
        15: "I-No_Symptom_Occurence",
        16: "I-No_Finding_Occurence",
        17: "I-Population_Group",
        18: "I-Age_Group",
    }

    # create an instance of the model and the tokenizer

    prediction = inference(model, tokenizer, case, ids_to_labels)

    pred_line = []

    for tok, pred in zip(case, prediction):
        pred_line.append((tok, pred))

    return pred_line


def inference(model, tokenizer, case, ids_to_labels):
    """
    Inference function

    Parameters
    ==========
    model: BertForTokenClassification
        Model used for the prediction
    tokenizer: BertTokenizerFast
        Tokenizer used for the prediction
    case: str
        Medical case
    ids_to_labels: dict{int:str}
        Dictionary containing all labels possible

    Returns
    =======
    prediction: List[str]
        List of the prediction made by the model
    """
    model.eval()
    inputs = tokenizer(
        case,
        is_split_into_words=True,
        return_offsets_mapping=True,
        padding="max_length",
        truncation=True,
        max_length=300,
        return_tensors="pt",
    )

    ids = inputs["input_ids"]
    mask = inputs["attention_mask"]
    outputs = model(ids, attention_mask=mask)
    logits = outputs[0]

    active_logits = logits.view(-1, model.num_labels)  # shape (batch_size * seq_len, num_labels)
    flattened_predictions = torch.argmax(
        active_logits, axis=1
    )  # shape (batch_size*seq_len,) - predictions at the token level

    tokens = tokenizer.convert_ids_to_tokens(ids.squeeze().tolist())
    token_predictions = [ids_to_labels[i] for i in flattened_predictions.cpu().numpy()]
    wp_preds = list(
        zip(tokens, token_predictions)
    )  # list of tuples. Each tuple = (wordpiece, prediction)

    prediction = []
    wp_preds_tmp = []
    for token_pred, mapping in zip(wp_preds, inputs["offset_mapping"].squeeze().tolist()):
        # only predictions on first word pieces are important
        if mapping[0] == 0 and mapping[1] != 0:
            prediction.append(token_pred[1])
            wp_preds_tmp.append((token_pred, token_pred[1]))
        else:
            wp_preds_tmp.append((token_pred, token_pred[1]))
            continue

    for i in range(len(prediction)):
        if prediction[i] == "B-Relevant_symptoms":
            prediction[i] = "B-Sign_or_Symptom"
        elif prediction[i] == "I-Relevant_symptoms":
            prediction[i] = "I-Sign_or_Symptom"
        logger.info(prediction[i])

    return prediction


def detokenizer(wp_preds):
    """

    Parameters
    ----------
    wp_preds: List[tuple]
        Tuples containing wordpiece token and the predicted label

    Returns
    -------
    detokenized_case: List[Str]
        List of string containing the detokenized case
    detokenized_preds: List[Str]
        List of string containing the detokenized predictions
    """
    detokenized_case = []
    detokenized_preds = []
    counter = 0
    # We iterate through the tokken
    for t, p in wp_preds:
        if t == "[CLS]":
            continue
        if t == "[SEP]":
            continue
        if t == "[PAD]":
            continue
        if t == "[MASK]":
            continue
        # We check if the token is a subword
        if "##" in t:
            t = t.replace("##", "")
            detokenized_case[counter - 1] = detokenized_case[counter - 1] + t
            label = detokenized_preds[counter - 1]
            if label == "O":
                if p != "O":
                    detokenized_preds[counter - 1] = p
            replaced = True
        else:
            replaced = False
            detokenized_case.append(t)
            detokenized_preds.append(p)
        if not replaced:
            counter += 1
        logger.debug(wp_preds)
    return detokenized_case, detokenized_preds
