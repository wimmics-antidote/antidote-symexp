

ANTIDOTE SYMEXP MODULE
========================

The repository is part of the ANTIDOTE Project on Explainable AI. For more information please check https://www.inria.fr/en/explainable-ai-algorithm-learning.

The SYMEXP Module is the basis for the [**SYM**ptoms **EXP**lanation tool]("http://antidote.i3s.unice.fr/symexp").

Requirements
------------

- The code was tested under Python 3.10.14. It might run under lower versions of Python, but it wasn't tested on those.
- The required packages are listed in [`requirements.txt`](./requirements.txt)
- The code eis heavily dependent on the following libraries:
  - [Pytorch](https://pytorch.org/) >=2: Developed with 2.2.2
  - [Hugging Face](https://huggingface.co/) >=4: Developed with 4.38.2
  - [OpenAI](https://platform.openai.com/overview) >=1: Developed with 1.13.3
- You will need to download the ```en_product4.xml``` file to have the HPO ontology and place it under the [`./HPO_Database`](./HPO_Database) directory.  You can do it on this website : https://www.orphadata.com/phenotypes/


Installation
------------

We recommend use some form of virtual environment first:

    $ python -m venv symexp-venv
    $ source ./symexp-venv/bin/activate
    (symexp-venv) $ pip install --upgrade pip

Before installing everything else, we recommend you to install your preferred PyTorch version. For exemple, if you are running this from a machine without GPU access, it's recommended to install PyTorch like :

    (symexp-venv) $ pip install torch==2.2.2 --index-url https://download.pytorch.org/whl/cpu

If you don't especially install the version of PyTorch you'd like it's probable that `pip` will try to install the default version for your system when resolving the dependencies (e.g. for Linux it's the GPU version)

You can then use the [`requirement.txt`](./requirements.txt) file to install other libraries using this command from the root of the project :

    (symexp-venv) $ pip install -r requirement.txt

Usage
-----

To use the pipeline, you should run the command at the root of the project:

    (symexp-venv) $ flask --app API_main run --debug --port 5000 --reload

It will launch the main program and the Flask API will be accessible at ```http://localhost:5000/```.

Documentation
---------

After using the Flask command, if you didn't change the port parameter, you can then access the documentation for the pipeline directly at
[http://localhost:5001/symexp/api/docs](http://localhost:5000/symexp/api/docs)

Request example
---------------

You can send request with ease to each endpoint using [Postman](https://www.postman.com/), where a collection is already built to make it faster for you to test.

https://planetary-equinox-230769.postman.co/workspace/SYMEXP-Pipeline~45c9cf46-6e2a-4ccf-852e-1a33b8b42bc1/collection/27549583-04f14728-72c6-44dd-b97b-9f3d66489cd0?action=share&creator=27549583
