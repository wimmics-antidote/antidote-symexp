"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from transformers import AutoTokenizer, AutoModelForTokenClassification
import Configuration as cfg
import pickle
from flair.embeddings import SentenceTransformerDocumentEmbeddings
import random
import numpy as np
import torch


def read_pickle_file(file_name):
    with open(file_name, "rb") as f:
        data = pickle.load(f)
    return data


config = {
    "case": "A previously healthy 34-year-old woman is brought to the physician because of fever and headache for 1 week. She has not been exposed to any disease. She takes no medications. Her temperature is 39.3°C (102.8°F), pulse is 104/min, respirations are 24/min, and blood pressure is 135/88 mm Hg. She is confused and oriented only to person. Examination shows jaundice of the skin and conjunctivae. There are a few scattered petechiae over the trunk and back. There is no lymphadenopathy. Physical and neurologic examinations show no other abnormalities. Test of the stool for occult blood is positive. Laboratory studies show: Hematocrit 32% with fragmented and nucleated erythrocytes Leukocyte count 12,500/mm3 Platelet count 20,000/mm3 Prothrombin time 10 sec Partial thromboplastin time 30 sec Fibrin split products negative Serum Urea nitrogen 35 mg/dL Creatinine 3.0 mg/dL Bilirubin  Total 3.0 mg/dL  Direct 0.5 mg/dL Lactate dehydrogenase 1000 U/L Blood and urine cultures are negative. A CT scan of the head shows no abnormalities. Which of the following is the most likely diagnosis?",  # noqa
    "ner_path": "Natfike/NER",
    "hpo_terms": read_pickle_file(
        "./Matching_Module/resources/hpo_embeddings_s_pubmedbert_ms_marco_scifact_embedding.pickle"
    ),
    "hpo_xml_path": "HPO_Database/en_product4.xml",
    "hpo_json_path": "HPO_Database/tmp.json",
    "hpo_clean_json_path": "HPO_Database/clean_hpo.json",
}


# Creation of the model and tokenizer for the NER
# Seed our run
device = "cuda" if torch.cuda.is_available() else "cpu"
torch.manual_seed(42)
seed = 42
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

# We create the tokenizer and the model for the NER
tokenizer = AutoTokenizer.from_pretrained(cfg.config["ner_path"])
model = AutoModelForTokenClassification.from_pretrained(cfg.config["ner_path"])
model = model.to(device)

# We create the model for the matching module
embedding_full_eval = SentenceTransformerDocumentEmbeddings(
    "pritamdeka/S-PubMedBert-MS-MARCO-SCIFACT"
)

config["ner_model"] = model
config["ner_tokenizer"] = tokenizer
config["matching_module_model"] = embedding_full_eval
