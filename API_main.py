"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import ask_api
import logging
from flask import Blueprint, Flask, request, jsonify
from flask_cors import CORS
from flask_restx import Api, Resource, fields
from flask_restx.apidoc import apidoc
from logging.config import dictConfig
from transformers import AutoTokenizer, AutoModelForTokenClassification
from assets import json_exemple
from NER import ner
from Matching_Module import matching_module as mm
from Configuration import config as cfg
import hpo_database
from cases_method import extract_symptoms, extract_findings, list_labels
from time import time
from Findings import findings
from werkzeug.middleware.proxy_fix import ProxyFix

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stderr",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["console"]},
    }
)

logger = logging.getLogger(__name__)

# We create the database from the .xml file
logger.info("Creating JSON from XML")
hpo_database.create_json(cfg["hpo_xml_path"], cfg["hpo_json_path"], cfg["hpo_clean_json_path"])

logger.info("Building DB from JSON")
hpo_database.create_db(cfg["hpo_clean_json_path"])


# This needs to be done before the Api instance for more information check:
# https://stackoverflow.com/questions/66158038/how-can-i-move-my-static-folder-to-an-url#-with-a-prefix-with-flask-restx
URL_PREFIX = "/symexp/api"
apidoc.url_prefix = URL_PREFIX
apidoc.static_url_path = f"{URL_PREFIX}/swaggerui"

app = Flask(__name__)
app.logger.info("Loading SYMEXP")

# Avoid strict slashes: https://stackoverflow.com/questions/40365390/trailing-slash-in-flask-route
app.url_map.strict_slashes = False

# Tell Flask is behind a proxy: https://flask.palletsprojects.com/en/3.0.x/deploying/proxy_fix/
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)
CORS(app)


@app.route("/symexp/health")
def health():
    """
    Route to check the application is "healthy" (for docker).
    """
    return jsonify("ok")


# We require a blueprint so the SWAGGER api docs are created under the /symexp/api path
symexp_api_blueprint = Blueprint("symexp", __name__, url_prefix=URL_PREFIX)
api = Api(
    symexp_api_blueprint,
    version="0.1",
    title="SYMEXP Pipeline API Documentation",
    description="Documentation for the SYMEXP Pipeline",
    default="SYMEXP Pipeline",
    doc="/docs",
)


# Models
pipeline_model = api.model(
    "Case",
    {
        "open_ai_key": fields.String(
            description="Enter your open_api API key to use findings", example=""
        ),
        "findings": fields.Boolean(
            description="To choose if you want to analyse findings (take some time)",
            example=False,
        ),
        "case": fields.String(description="The case description", example=cfg["case"]),
        "correctAnswer": fields.String(
            description="The correct disease answer",
            example="Thrombotic thrombocytopenic purpura",
        ),
        "incorrectAnswers": fields.List(
            fields.String,
            description="Incorrect disease answers",
            example=["Bladder cancer", "Boutonneuse fever"],
        ),
    },
)

output_pipeline_model = api.model(
    "Output",
    {
        "why": fields.String(
            description="Explanation for the correct diagnosis",
            example=json_exemple.why_template,
        ),
        "why_not": fields.Raw(
            description="Explanation for each incorrect diagnosis",
            example=json_exemple.why_not_template,
        ),
        "further": fields.String(
            description="Further explanation", example=json_exemple.further_template
        ),
    },
)

ner_model = api.model(
    "NER",
    {
        "case": fields.String(description="The case description", example=cfg["case"]),
        "model": fields.String(description="If you want to use another model", example=""),
        "token": fields.String(description="The token if the model is private", example=""),
    },
)

matching_model = api.model(
    "Matching Module",
    {
        "findings": fields.Boolean(
            description="To choose if you want to analyse findings (take some time)",
            example=False,
        ),
        "case": fields.Raw(description="", example=json_exemple.exemple["case"]),
    },
)

explanation_model = api.model(
    "Explanation",
    {
        "findings": fields.Boolean(
            description="To choose if you want to analyse findings (take some time)",
            example=False,
        ),
        "case": fields.Raw(description="", example=json_exemple.exemple["case"]),
    },
)

findings_model = api.model(
    "Findings",
    {
        "open_ai_key": fields.String(
            description="Enter your open_api API key to use findings", example=""
        ),
        "detected_findings": fields.Raw(
            description="all detected findings", example=json_exemple.detected_findings
        ),
    },
)

triplet_model = api.model(
    "Triplets",
    {
        "case": fields.String(description="", example=json_exemple.triplets_exemple["case"]),
        "correct disease": fields.Raw(
            description="", example=json_exemple.triplets_exemple["correct disease"]
        ),
        "incorrect_diseases": fields.Raw(
            description="", example=json_exemple.triplets_exemple["incorrect_diseases"]
        ),
        "word_labels": fields.Raw(
            description="", example=json_exemple.triplets_exemple["word_labels"]
        ),
        "detected_symptoms": fields.Raw(
            description="", example=json_exemple.triplets_exemple["detected_symptoms"]
        ),
    },
)

searchDiseaseByInclusion_model = api.model(
    "searchDiseaseByName",
    {
        "input": fields.String(description="", example="Alexander"),
        "limit": fields.Integer(description="", example=4),
    },
)


# Routes
@api.route("/pipeline")
class Pipeline(Resource):
    @api.doc(description="Process a clinical case and provide explanations", tags="test")
    @api.expect(pipeline_model)
    @api.response(200, "Success", output_pipeline_model)
    def post(self):
        start = time()
        data = request.get_json()

        findings = True

        if "open_ai_key" in list(data):
            if data["open_ai_key"]:
                key = data["open_ai_key"]
            else:
                key = ""
        else:
            key = ""

        # We get the symptoms for the correct and incorrect diseases
        correct_symptoms = hpo_database.get_symptoms_for_diseases([data["correctAnswer"]])
        incorrect_symptoms = hpo_database.get_symptoms_for_diseases(data["incorrectAnswers"])

        # We create a variable to regroup all information about the case
        case_ = {
            "case": data["case"],
            "correct disease": {
                "name": data["correctAnswer"],
                "symptoms": correct_symptoms[data["correctAnswer"]],
            },
            "incorrect_diseases": {},
        }
        for i, incorrect_answer in enumerate(data["incorrectAnswers"]):
            case_["incorrect_diseases"][i + 1] = {
                "name": incorrect_answer,
                "symptoms": incorrect_symptoms[incorrect_answer],
            }

        detected_symptoms = ask_api.ask_ner(case_["case"])

        # We extract only the "*-Relevant-Symptoms"
        case_["word_labels"] = list_labels(detected_symptoms["labels"][0])
        case_["detected_symptoms"] = {"detected": extract_symptoms(detected_symptoms["labels"][0])}

        if findings:
            case_["detected_findings"] = {
                "detected": extract_findings(detected_symptoms["labels"][0])
            }
            case_["detected_findings"]["predictions"] = ask_api.ask_findings(
                case_["detected_findings"]["detected"], key
            )

        if findings:
            (
                case_["detected_symptoms"]["matched"],
                case_["detected_findings"]["matched"],
            ) = ask_api.ask_matching_module(case_, True)
        else:
            case_["detected_symptoms"]["matched"] = ask_api.ask_matching_module(case_, False)

        explanations = ask_api.ask_explanation(case_, findings)

        app.logger.debug(time() - start)
        return explanations


@api.route("/ner")
class NER(Resource):
    @api.doc(description="Perform Named Entity Recognition (NER) on a clinical case")
    @api.expect(ner_model)
    @api.response(200, "Success")
    def post(self):
        case_ = request.get_json()
        # We check to see if we need to use another model from the request
        if "model" in case_ and case_["model"] != "":
            if "token" not in case_ or case_["token"] == "":
                case_["token"] = ""
            tokenizer = AutoTokenizer.from_pretrained(case_["model"], use_auth_token=case_["token"])
            model = AutoModelForTokenClassification.from_pretrained(
                case_["model"], use_auth_token=case_["token"]
            )
            result = ner.main([case_["case"]], model, tokenizer)
        # else we use our own model
        else:
            result = ner.main([case_["case"]], cfg["ner_model"], cfg["ner_tokenizer"])
        send_result = {"cases": result[0], "labels": result[1]}
        return jsonify(send_result)


@api.route("/findings")
class Findings(Resource):
    @api.doc(description="Perform the ... of the findings")
    @api.expect(findings_model)
    def post(self):
        data = request.get_json()
        detected_findings = data["detected_findings"]
        key = data["open_ai_key"]
        outputs = findings.main(detected_findings, key)
        return outputs


@api.route("/matchingmodule")
class MatchingModule(Resource):
    @api.doc(description="Apply the matching module on a clinical case")
    @api.expect(matching_model)
    def post(self):
        data = request.get_json()
        case_ = data["case"]
        findings = data["findings"]
        outputs = mm.main(cfg["hpo_terms"], case_, cfg["matching_module_model"], findings)
        return jsonify(outputs)


@api.route("/explanation")
class Explanation(Resource):
    @api.doc(description="Generate explanations based on final results")
    @api.expect(explanation_model)
    def post(self):
        data = request.get_json()
        case_ = data["case"]
        findings = data["findings"]
        same = []
        same_findings = []
        further_explanation = []
        matches = []

        correct_symptoms = case_["correct disease"]["symptoms"]["list_symptoms"]
        correct_frequence = case_["correct disease"]["symptoms"]["list_frequence"]
        for i in range(len(correct_symptoms)):
            symptom = correct_symptoms[i]
            if symptom in case_["detected_symptoms"]["matched"]:
                index = case_["detected_symptoms"]["matched"].index(symptom)
                if symptom.lower() != case_["detected_symptoms"]["detected"][index]:
                    same.append(symptom + f' ({case_["detected_symptoms"]["detected"][index]})')
                else:
                    same.append(symptom)
                matches.append(symptom)
            elif (
                correct_frequence[i] == "Frequent (79-30%)"
                or correct_frequence[i] == "Very frequent (99-80%)"
            ):
                further_explanation.append(correct_symptoms[i].lower())

        if findings:
            i = 0
            for i in range(len(correct_symptoms)):
                symptom = correct_symptoms[i]
                if symptom in case_["detected_findings"]["matched"]:
                    index = case_["detected_findings"]["matched"].index(symptom)
                    if symptom.lower() != case_["detected_findings"]["detected"][index]:
                        same_findings.append(
                            symptom + f' ({case_["detected_findings"]["detected"][index]})'
                        )
                    else:
                        same_findings.append(symptom)
                    matches.append(symptom)

        for symptoms in matches:
            if symptoms in further_explanation:
                further_explanation.remove(symptoms)

        why_not = {}
        # we iterate for each incorrect disease
        for keyDisease in case_["incorrect_diseases"]:
            incorrect_symptoms = case_["incorrect_diseases"][keyDisease]["symptoms"][
                "list_symptoms"
            ]
            incorrect_frequence = case_["incorrect_diseases"][keyDisease]["symptoms"][
                "list_frequence"
            ]

            name = case_["incorrect_diseases"][keyDisease]["name"]
            match = []
            not_matched = []
            obligatory_not_matched = []
            very_frequent_not_matched = []
            for i in range(len(incorrect_symptoms)):
                symptom = incorrect_symptoms[i]
                if symptom in matches:
                    match.append(symptom)
                else:
                    not_matched.append(symptom)
                    # we check if the missing symptom is very frequent or obligatory
                    if incorrect_frequence[i] == "Very frequent (99-80%)":
                        very_frequent_not_matched.append(symptom)

            second_part = ""
            if obligatory_not_matched:
                second_part += f"Moreover, {obligatory_not_matched} are obligatory symptoms "
            if very_frequent_not_matched:
                # Check if we already have text to make the phrase grammatically correct
                if second_part:
                    second_part += "and "
                else:
                    second_part += " Moreover"
                # add the very_frequent part to the text
                second_part += f"{very_frequent_not_matched} are very frequent symptoms "
            if very_frequent_not_matched or obligatory_not_matched:
                second_part += (
                    "for {" + name + "}, and they are not present in the case description"
                )

            despite = ""
            if match:
                despite = (
                    f'", despite {match} shared with the correct diagnosis"'
                    + "{"
                    + case_["correct disease"]["name"]
                    + "}"
                )

            # we add to the list the entire explanation for the incorrect disease
            why_not[keyDisease] = (
                "As for the {"
                + name
                + "} diagnosis, it has to be discarded because the patient is not showing "
                + str(not_matched)
                + ", symptoms that cannot be found in the case description"
                + despite
                + second_part
            )

        # We create the "why" explication...
        why = (
            "The patient is showing a {"
            + case_["correct disease"]["name"]
            + "} as these following symptoms: "
            + str(same)
            + "are direct symptoms of the {"
            + case_["correct disease"]["name"]
            + "}."
        )
        # ...and add the findings if we have them
        if findings:
            why += (
                f" Using the findings, we also found that "
                f"the patient is showing these symptoms : {same_findings}"
            )
        # We create the "further" explication...
        further = (
            f"Furthermore, {further_explanation} are also frequent symptoms for "
            + "{"
            + case_["correct disease"]["name"]
            + "}"
        )
        # ... and add a little text if we didn't use findings
        if not findings:
            further += " and could be found in the findings of the clinical case"

        output = {
            "why": why,
            "why_not": why_not,
            "further": further,
        }

        return output


@api.route("/triplets")
class Triplets(Resource):
    @api.doc(description="Generate triplets")
    @api.expect(triplet_model)
    def post(self):
        data = request.get_json()
        words_list = data["case"].split()
        words_labels_list = data["word_labels"]

        # We generate the triplet (patient, isA, populationGroup)
        age_group_positions = [i for i, x in enumerate(words_labels_list) if "Age_Group" in x]
        population_group_positions = [
            i for i, x in enumerate(words_labels_list) if "Population_Group" in x
        ]
        patient_positions = age_group_positions + population_group_positions
        patient_positions.sort()
        patient_words = [words_list[k] for j, k in enumerate(patient_positions)]
        patient = " ".join(patient_words)
        data["patient"] = patient

        # We generate the triplet (patient, has/n't, disease)
        triplet_patient_disease = []
        triplet_patient_disease.append([patient, "have", data["correct disease"]["name"]])
        for incorrect in data["incorrect_diseases"]:
            triplet_patient_disease.append(
                [patient, "don't have", data["incorrect_diseases"][incorrect]["name"]]
            )

        # We generate the triplet (patient, has, symptom)
        triplet_patient_symptom = []
        symptoms_list = []
        for i, words_label in enumerate(words_labels_list):
            if words_label == "B-Relevant_symptoms":
                symptoms_list.append([words_list[i]])
            elif words_label == "I-Relevant_symptoms":
                symptoms_list[-1].append(words_list[i])
        symptoms = []
        # we clean the results
        for i, symptom in enumerate(symptoms_list):
            cleanned_symptom = " ".join(symptom).replace(".", "").replace(",", "")
            symptoms.append(cleanned_symptom)
            triplet_patient_symptom.append([patient, "have", cleanned_symptom])

        # We generate the triplet (disease, has, symptom)
        triplet_disease_symptom = []
        for incorrect in data["incorrect_diseases"]:
            symptoms = data["incorrect_diseases"][incorrect]["symptoms"]
            for term in symptoms[0]["terms"]:
                triplet_disease_symptom.append(
                    [
                        data["incorrect_diseases"][incorrect]["name"],
                        "have",
                        term["name"],
                    ]
                )
        app.logger.debug(triplet_disease_symptom)

        triplet = {
            "triplet_patient_disease": triplet_patient_disease,
            "triplet_patient_symptom": triplet_patient_symptom,
            "triplet_disease_symptom": triplet_disease_symptom,
        }

        return triplet


@api.route("/disease")
class searchDiseaseByInclusion(Resource):
    @api.doc(description="Get diseases by approximate name")
    @api.expect(searchDiseaseByInclusion_model)
    def post(self):
        data = request.get_json()
        diseases_list = hpo_database.searchDiseaseByInclusion(data["input"], data["limit"])
        return diseases_list


app.register_blueprint(symexp_api_blueprint)
app.logger.info("SYMEXP Loaded")
