"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import requests
import os


def _build_url(service: str) -> str:
    """Helper function to build a url based on the current host"""
    port = os.environ.get("INTERNAL_HTTP_SERVER_PORT", 5001)
    return f"http://localhost:{port}/symexp/api/{service}"


def ask_ner(case):
    """Helper function to send a request to the /ner endpoint"""
    param = {"case": case}
    reponse = requests.post(_build_url("ner"), json=param)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return reponse.status_code


def ask_matching_module(case, findings=False):
    """Helper function to send a request to the /matchingmodule endpoint"""
    data = {"case": case, "findings": findings}
    reponse = requests.post(_build_url("matchingmodule"), json=data)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return reponse.status_code


def ask_findings(detected, key):
    """Helper function to send a request to the /findings endpoint"""
    data = {"detected_findings": detected, "open_ai_key": key}
    reponse = requests.post(_build_url("findings"), json=data)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return reponse.status_code


def ask_explanation(case, findings=False):
    """Helper function to send a request to the /explanation endpoint"""
    data = {"case": case, "findings": findings}
    reponse = requests.post(_build_url("explanation"), json=data)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return reponse.status_code


def ask_triplets(case):
    """Helper function to send a request to the /triplets endpoint"""
    reponse = requests.post(_build_url("triplets"), json=case)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return reponse.status_code
