import logging

"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

"""

"""


logger = logging.getLogger(__name__)


def generate_why(templates, case):
    disease = case["correct disease"]["name"].lower()
    # We create a list with matching symptoms
    tmp = list(filter(lambda x: x != "o", case["detected_symptoms"]["matched"]))
    # if there is no matching symptoms, return X
    if len(tmp) == 0:
        return "X"
    elif len(tmp) == 1:
        symptoms = tmp[0]
    # If we have multiple matching symptoms, we create a string with all of them
    elif len(tmp) > 1:
        symptoms = ", ".join(tmp[:-1])
        symptoms += " and " + tmp[-1]
    # Replace the template with the symptoms
    explanation = (
        templates["why"]
        .replace("[CORRECT DIAGNOSIS]", disease)
        .replace("[MATCHED SYMPTOMS]", symptoms)
    )
    return explanation


def generate_why_not(templates, diseases_symptoms, case):
    test = []
    correct = case["correct disease"]["name"]
    logger.info(correct)
    logger.info(diseases_symptoms)
    for incorrect in case["incorrect_diseases"]:
        option = case["incorrect_diseases"][incorrect]["name"]
        if correct in diseases_symptoms:
            if option.lower != correct:
                disease = option
                shared_symptoms = []
                missing_symptoms = []
                logger.info(diseases_symptoms[correct])
                for symptom in diseases_symptoms[correct]:
                    if symptom in case["detected_symptoms"]["matched"]:
                        shared_symptoms.append(symptom)
                    else:
                        missing_symptoms.append(symptom)

        part_1 = ""
        part_2 = ""

        # Add the first part with missing symptoms
        if len(missing_symptoms) > 0:
            if len(missing_symptoms) == 1:
                part_1 = shared_symptoms[0]
            elif len(missing_symptoms) > 1:
                part_1 = ", ".join(missing_symptoms[:-1])
                part_1 += " and " + missing_symptoms[-1]
        else:
            return "X"

        explanation = (
            templates["why_not_1"]
            .replace("[INCORRECT DIAGNOSIS]", disease)
            .replace("[INCORRECT DIAGNOSIS SYMPTOMS]", part_1)
        )
        logger.info(explanation)
        # Add the second part with shared symptoms
        if len(shared_symptoms) > 0:
            if len(shared_symptoms) == 1:
                part_2 = shared_symptoms[0]
            elif len(shared_symptoms) > 1:
                part_2 = ", ".join(shared_symptoms[:-1])
                part_2 += " and " + shared_symptoms[-1]
            explanation += " " + templates["why_not_2"].replace(
                "[SHARED CORRECT SYMPTOMS]", part_2
            ).replace("[CORRECT DIAGNOSIS]", correct).replace(
                "[INCORRECT DIAGNOSIS]", disease
            ).replace(
                "[INCORRECT DIAGNOSIS SYMPTOMS]", part_1
            )

        test.append(explanation)
    logger.debug(test)
