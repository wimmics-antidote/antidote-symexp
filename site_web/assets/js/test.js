function autocomplete(inp) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    let currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function() {
        let a, b, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        if (val.length>3) {
            const url = "https://hpo.jax.org/api/hpo/search";
            const params = {q: val, category: "diseases", max:4};

            let diseases;
            $.get(url, params)
                .done(response => {
                    response["diseases"].forEach(function(disease){
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + disease["dbName"] + " | " + disease["diseaseId"] + "</strong>";
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + disease["dbName"]  + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    });
                })
                .fail(error => {
                    console.log(error)
                });
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        let x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode === 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode === 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode === 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt !== x[i] && elmnt !== inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}



/*An array containing all the country names in the world:*/

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("incorrect-1"));
autocomplete(document.getElementById("incorrect-2"));
autocomplete(document.getElementById("incorrect-3"));
autocomplete(document.getElementById("incorrect-4"));
autocomplete(document.getElementById("correct-answer-input"));

document.querySelector("form").addEventListener('submit', (event)=>{
    event.preventDefault();
    // Get the values of the four incorrect answers
    const formData = new FormData(event.target)
    let form = Object.fromEntries(formData.entries());
    const incorrectAnswers = [];
    if (form.incorrect_answer_1 !== ""){
        incorrectAnswers.push(form.incorrect_answer_1);
    }
    if (form.incorrect_answer_2 !== ""){
        incorrectAnswers.push(form.incorrect_answer_2);
    }
    if (form.incorrect_answer_3 !== ""){
        incorrectAnswers.push(form.incorrect_answer_3);
    }
    if (form.incorrect_answer_4 !== ""){
        incorrectAnswers.push(form.incorrect_answer_4);
    }
    let findings = !document.querySelector(".slider").classList.contains("left");

    let key = document.getElementById("openai-key").value;

    // Group the values into an object
    const data = {
        case : form.cases,
        correctAnswer : form.correct_answer,
        incorrectAnswers: incorrectAnswers,
        findings : findings,
        open_ai_key : key
    };
    console.log(data)
    const request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:5001/symexp/api/pipeline');
    request.setRequestHeader('Content-Type', 'application/json');
    request.onload = () => {
        if (request.status === 200) {
            console.log(request.response);
            // Change the view
            changeView(request.response);
            // Hide the div "ask" and show the div "resp"
            document.getElementById("ask").style.display = "none";
            document.getElementById("resp").style.display = "block";
        }
    };
    // Send the object as JSON
    request.send(JSON.stringify(data));
});

function highlightEntities(text) {
    return text.replace(/\[.*?\]/g, '<span class="highlight">$&</span>');
}

function changeView(data){
    // Test if test.why exists and contains a value
    let test = JSON.parse(data);
    if (test.why) {
        document.getElementById('why').innerHTML = '<h2>Why</h2><p>' + highlightEntities(test.why) + '</p>';
    } 
    // Test if test.why_not and contains a value
    if (test.why_not) {
        document.getElementById('why_not').innerHTML =
            '<h2>Why not</h2>';
        // Loop through the why_not values length
        for (let i = 1; i <= Object.keys(test.why_not).length; i++) {
            document.getElementById('why_not').innerHTML += '<p>' + highlightEntities(test.why_not[i]) + '</p>';
        }
    }
    // Test if test.further exists and contains a value
    if (test.further) {
        document.getElementById('further').innerHTML = '<h2>Futhermore</h2><p>' + highlightEntities(test.further) + '</p>';
    }
}

document.getElementById("example").addEventListener("click",()=>{
    document.getElementById("clinical-cases").value = "A previously healthy 34-year-old woman is brought to the physician because of fever and headache for 1 week. She\n" +
        "has not been exposed to any disease. She takes no medications. Her temperature is 39.3°C (102.8°F), pulse is\n" +
        "104/min, respirations are 24/min, and blood pressure is 135/88 mm Hg. She is confused and oriented only to person.\n" +
        "Examination shows jaundice of the skin and conjunctivae. There are a few scattered petechiae over the trunk and\n" +
        "back. There is no lymphadenopathy. Physical and neurologic examinations show no other abnormalities. Test of the\n" +
        "stool for occult blood is positive. Laboratory studies show:\n" +
        "Hematocrit 32% with fragmented and nucleated erythrocytes\n" +
        "Leukocyte count 12,500/mm3\n" +
        "Platelet count 20,000/mm3\n" +
        "Prothrombin time 10 sec\n" +
        "Partial thromboplastin time 30 sec\n" +
        "Fibrin split products negative\n" +
        "Serum\n" +
        "Urea nitrogen 35 mg/dL\n" +
        "Creatinine 3.0 mg/dL\n" +
        "Bilirubin\n" +
        " Total 3.0 mg/dL\n" +
        " Direct 0.5 mg/dL\n" +
        "Lactate dehydrogenase 1000 U/L\n" +
        "Blood and urine cultures are negative. A CT scan of the head shows no abnormalities. Which of the following is the\n" +
        "most likely diagnosis?"
    document.getElementById("correct-answer-input").value = "Thrombotic thrombocytopenic purpura"
    document.getElementById("incorrect-1").value = "Immune thrombocytopenic purpura"
    document.getElementById("incorrect-2").value = "Meningococcal meningitis"
    document.getElementById("incorrect-3").value = "Sarcoidosis"
    document.getElementById("incorrect-4").value = "Systemic lupus erythematosus"
})

$(document).ready(function() {
    $('.switch').on("mouseup",function() {
        var slider = $(this).find('.slider');
        var additionalInputs = $('.additional-inputs');
        var isSwitchOn = !slider.hasClass('left');
        console.log(isSwitchOn)
        if (isSwitchOn) {
            slider.addClass('left');
            additionalInputs.slideUp();
        } else {
            slider.removeClass('left');
            additionalInputs.slideDown();
        }

    });
});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}