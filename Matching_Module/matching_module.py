"""
The pipeline module hold different functions to run a full annotation pipeline.

   Copyright 2023 The ANTIDOTE Project Contributors <https://univ-cotedazur.eu/antidote>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import numpy as np
from flair.data import Sentence
from scipy.spatial import distance_matrix


# ------------------------------------------------------------------------------


def main(hpo_terms, input, embedding_full_eval, findings):
    """
    Main function for the matching module

    Parameters
    ==========
    hpo_terms: dict
        Dictionary with hpo_terms

    input: dict
        Dictionnary containing : case, correct disease, incorrect_diseases,
        world_labels, detected_symptoms, detected_findings

    embedding_full_eval: SentenceTransformerDocumentEmbeddings
        The transformer used for the embeddings

    findings: Boolean
        Boolean to know if we are supposed to use the findings

    Returns
    =======
    bests_s: List[Str]
        List of the matching symptoms

    bests_f: List[Str]
        List of the matching findings
    """
    method_full_eval = "sentences"
    if method_full_eval == "sentences":
        bests_s = match_with_context(input, embedding_full_eval, hpo_terms)
        if not findings:
            return bests_s
        bests_f = match_with_context(input, embedding_full_eval, hpo_terms, findings=True)

    return bests_s, bests_f


def match_with_context(case, embeddings, hpo_terms, findings=False):
    """


    Parameters
    ==========
    case: str
        Medical case
    embeddings: SentenceTransformerDocumentEmbeddings
        The transformer used for the embeddings
    hpo_terms: dict
        Dictionary with hpo_terms
    findings: Boolean
        Boolean to know if we are supposed to use the findings

    Returns
    =======
    matches: List[Str]
        List of matching terms
    """
    five_bests_for_detected = []

    if findings:
        symptoms_to_match = case["detected_findings"]["predictions"]
        context_symptoms = case["detected_findings"]["detected"]
    else:
        symptoms_to_match = case["detected_symptoms"]["detected"]
        context_symptoms = case["detected_symptoms"]["detected"]

    for i, detectedSymptom in enumerate(symptoms_to_match):
        detectedSymptom = detectedSymptom.lower()
        if not context_symptoms:
            return []
        context_symptom = context_symptoms[i].lower()
        # Split sentences of the case
        sentences = case["case"].lower().split(". ")
        findInSentence = ""

        # Find the context of the detected symptom
        # ### V1
        for sentence in sentences:
            sentence += "."
            if context_symptom in sentence:
                findInSentence = sentence
                just_context = sentence.replace(context_symptom, "")
                break
        if findInSentence != "":
            just_context_sentence = Sentence(just_context)
            embeddings.embed(just_context_sentence)
            detected_symptom_sentence = Sentence(detectedSymptom)
            embeddings.embed(detected_symptom_sentence)
            # NO CONTEXT
            detected_context_embedding = combine_embeddings(
                just_context_sentence.embedding.cpu(),
                detected_symptom_sentence.embedding.cpu(),
            )

            hpo_term_names = list(hpo_terms.values())
            hpo_sentence_embeddings = np.array(
                [term["sentence"].embedding.cpu().numpy() for term in hpo_term_names]
            )

            # Note: This assumes combine_embeddings can handle numpy arrays.
            combined_embeddings = combine_embeddings(
                just_context_sentence.embedding.cpu().numpy(), hpo_sentence_embeddings
            )

            detected_context_embedding = np.array([detected_context_embedding.numpy()])

            # Calculate the distance matrix.
            dist = distance_matrix(detected_context_embedding, combined_embeddings)
            best = np.argsort(dist[0])[:5]
            bests = []
            list_hpo_context_embeddings_names = []
            for hpo_term in hpo_terms:
                list_hpo_context_embeddings_names.append(hpo_terms[hpo_term])
            for index in best:
                bests.append([dist[0][index], list_hpo_context_embeddings_names[index]])
            five_bests = []
            for j, b in enumerate(bests):
                five_bests.append(b[1]["name"])
            five_bests_for_detected.append(five_bests)
    matches = []
    for best_matches in five_bests_for_detected:
        matches.append(best_matches[0])
    return matches


def combine_embeddings(embeddings1, embeddings2):
    return np.add(embeddings1, embeddings2)
